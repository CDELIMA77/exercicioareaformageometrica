package com.company;

import java.util.ArrayList;

public class Main {

    // Crie um sistema que permite a criação de formas geométricas e o calcula de suas áreas.
    // O sistema deve atender os seguintes requistos
    // Deve se ter coom formas obrigatórias o circulo, triangulo e retangulo
    // Para criar uma forma , basta que seja informado o tamanho dos seus lados
    // após criada uma forma, não é possivel alterar o tamanho de seus lados
    // No caso do triangulo , deve-se verificar se os lados informados geram um triangulo valido. Caso
    // contrário, responder um erro com sout
    // utilizar o proprio codigo fonte para instanciar as 3 formas e exibir suas áreas no console
    //
    // Versao 2 - Aprimorar o sistema para que o usário possa informar , atraves do terminal, qual forma deseja criar
    // e o tamanho de seus lados
    // Caso o usuario informe mais de 4 lados, responder erro no terminal
    // + O sistema deve informar a area de qualquer forma já criada

    public static void main(String[] args) {
        // Leitora
        ArrayList<Double> valorLados = LeitorFaces.definirFaces();

        // Calculadora
        double areaI;
        CalculadoraArea area = new CalculadoraArea();
        areaI = area.calcular(valorLados);

        // Impressora
        Impressora impressora = new Impressora();
        impressora.imprimir(areaI);

    }
}
