package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class CalculadoraArea {
    public static double calcular(ArrayList<Double> valorLados) {

        if(valorLados.size() == 1 ){
            Circulo circulo = new Circulo(valorLados.get(0));
            return circulo.calcularArea();
        }

        if(valorLados.size() == 2 ){
            Retangulo retangulo = new Retangulo(valorLados.get(0),valorLados.get(1));
            return retangulo.calcularArea();
        }

        if(valorLados.size() == 3 ){
            Triangulo triangulo = new Triangulo(valorLados.get(0),valorLados.get(1),valorLados.get(2));
            return triangulo.calcularArea();
        }

        System.out.println("Forma escolhida não possui definição de área.");
        return 0.00;
    }
}
