package com.company;

public class Triangulo {
    private double lado1;
    private double lado2;
    private double lado3;

    public Triangulo(double lado1, double lado2, double lado3) {
        this.lado1 = lado1;
        this.lado2 = lado2;
        this.lado3 = lado3;
    }

    public double getLado1() {
        return lado1;
    }

    public double getLado2() {
        return lado2;
    }

    public double getLado3() {
        return lado2;
    }

    public double calcularArea(){
        if (((lado1 + lado2) > lado3) && ((lado1 + lado3) > lado2 ) && ((lado2 + lado3) > lado1 ) ) {
            double s = ( lado1 + lado2 + lado3 )/2;
            return Math.sqrt(s*(s-lado1)*(s-lado2)*(s-lado3));
        } else {
            System.out.println("Lados informados para triangulo inválidos");
            return 0.00;
        }
   }
}