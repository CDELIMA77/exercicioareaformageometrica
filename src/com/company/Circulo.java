package com.company;

public class Circulo {
    // Se coloca private nenhuma classe externa consegue nem ler nem gravar, como se não existisse
    private double raio;

    public Circulo(double raio) {
        this.raio = raio;
    }

    public double getRaio() {
        return raio;
    }

    public double calcularArea(){
        return Math.PI * Math.pow(raio, 2);
    }
}
