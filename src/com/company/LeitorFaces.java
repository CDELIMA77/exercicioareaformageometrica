package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class LeitorFaces {

    public static ArrayList<Double> definirFaces() {
        Scanner leitor = new Scanner(System.in);
        ArrayList<Double> valorLados = new ArrayList<>();

        System.out.println("Digite a quantidade de lados da figura geométrica que deseja calcular a área : ");
        int qtdFaces = leitor.nextInt();

        for ( int i = 0; i < qtdFaces; i++) {
            System.out.println("Digite valor lado " + (i+1) + " :");
            double face = leitor.nextDouble();
            valorLados.add(face);
        }

        return valorLados;
    }
}
